# sina

#### 项目介绍
主要用于个人初学python和scrapy框架的练手项目。该爬虫仅供学习使用，不用做任何其他途径。

使用urllib 模块爬取新浪财经的最新消息 将爬取得最新消息借助于itchat模块推送给指定好友

#### 开发依赖
* python3.6.1
* itchat
* scrapy 1.50


